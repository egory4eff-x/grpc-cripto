package server

import (
	"context"
	"fmt"
	"gitlab.com/egory4eff-x/grpc-crypto/config"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"net"
)

type GRPCServer struct {
	conf   config.RPCServer
	logger *zap.Logger
	srv    *grpc.Server
}

func NewGRPCServer(conf config.RPCServer, srv *grpc.Server, logger *zap.Logger) *GRPCServer {
	return &GRPCServer{conf: conf, srv: srv, logger: logger}
}

func (g *GRPCServer) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		var l net.Listener
		l, err = net.Listen("tcp", fmt.Sprintf(":%s", g.conf.Port))
		if err != nil {
			g.logger.Error("grpc server register error", zap.Error(err))
			chErr <- err
		}

		g.logger.Info("grpc server started", zap.String("port", g.conf.Port))

		if err = g.srv.Serve(l); err != nil {
			chErr <- err
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
		g.srv.GracefulStop()
	}

	return err
}
