package models

import (
	"time"

	"gitlab.com/egory4eff-x/grpc-crypto/internal/infrastructure/db/types"
)

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index

type CryptoMinDTO struct {
	ID        int               `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name      types.NullString  `json:"name" db:"name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	BuyPrice  types.NullString  `json:"buy_price" db:"buy_price" db_type:"varchar(200)" db_default:"default null"  db_ops:"create,update"`
	SellPrice types.NullString  `json:"sell_price" db:"sell_price" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	LastTrade types.NullString  `json:"last_trade" db:"last_trade" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	High      types.NullFloat64 `json:"high" db:"high" db_type:"numeric" db_default:"default 0" db_ops:"create,update"`
	Low       types.NullFloat64 `json:"low" db:"low" db_type:"numeric" db_default:"default 0" db_ops:"create,update"`
	Avg       types.NullString  `json:"avg" db:"avg" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	Vol       types.NullString  `json:"vol" db:"vol" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	VolCurr   types.NullString  `json:"vol_curr" db:"vol_curr" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	Updated   int               `json:"updated" db:"updated" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	CreatedAt time.Time         `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_ops:"created_at"`
	UpdatedAt time.Time         `json:"updated_at" db:"updated_at" db_type:"timestamp" db_default:"default (now()) not null" db_ops:"updated_at"`
	DeletedAt types.NullTime    `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_ops:"deleted_at"`
}

func (c *CryptoMinDTO) TableName() string {
	return "cryptomin"
}

func (c *CryptoMinDTO) OnCreate() []string {
	return []string{}
}

func (c *CryptoMinDTO) SetID(id int) *CryptoMinDTO {
	c.ID = id
	return c
}

func (c *CryptoMinDTO) GetID() int {
	return c.ID
}

func (c *CryptoMinDTO) SetName(name string) *CryptoMinDTO {
	c.Name = types.NewNullString(name)
	return c
}

func (c *CryptoMinDTO) GetName() string {
	return c.Name.String
}

func (c *CryptoMinDTO) SetBuyPrice(BuyPrice string) *CryptoMinDTO {
	c.BuyPrice = types.NewNullString(BuyPrice)
	return c
}

func (c *CryptoMinDTO) GetBuyPrice() string {
	return c.BuyPrice.String
}

func (c *CryptoMinDTO) SetSellPrice(SellPrice string) *CryptoMinDTO {
	c.SellPrice = types.NewNullString(SellPrice)
	return c
}

func (c *CryptoMinDTO) GetSellPrice() string {
	return c.SellPrice.String
}

func (c *CryptoMinDTO) SetLastTrade(LastTrade string) *CryptoMinDTO {
	c.LastTrade = types.NewNullString(LastTrade)
	return c
}

func (c *CryptoMinDTO) GetLastTrade() string {
	return c.LastTrade.String
}

func (c *CryptoMinDTO) SetHigh(High float64) *CryptoMinDTO {
	c.High = types.NewNullFloat64(High)
	return c
}

func (c *CryptoMinDTO) GetHigh() float64 {
	return c.High.Float64
}

func (c *CryptoMinDTO) SetUpdated(Updated int) *CryptoMinDTO {
	c.Updated = Updated
	return c
}

func (c *CryptoMinDTO) GetUpdated() int {
	return c.Updated
}

func (c *CryptoMinDTO) SetLow(Low float64) *CryptoMinDTO {
	c.Low = types.NewNullFloat64(Low)
	return c
}

func (c *CryptoMinDTO) GetLow() float64 {
	return c.Low.Float64
}

func (c *CryptoMinDTO) SetAvg(Avg string) *CryptoMinDTO {
	c.Avg = types.NewNullString(Avg)
	return c
}

func (c *CryptoMinDTO) GetAvg() string {
	return c.Avg.String
}

func (c *CryptoMinDTO) SetVol(Vol string) *CryptoMinDTO {
	c.Vol = types.NewNullString(Vol)
	return c
}

func (c *CryptoMinDTO) GetVol() string {
	return c.Vol.String
}

func (c *CryptoMinDTO) SetVolCurr(VolCurr string) *CryptoMinDTO {
	c.VolCurr = types.NewNullString(VolCurr)
	return c
}

func (c *CryptoMinDTO) GetVolCurr() string {
	return c.VolCurr.String
}

func (c *CryptoMinDTO) SetCreatedAt(createdAt time.Time) *CryptoMinDTO {
	c.CreatedAt = createdAt
	return c
}

func (c *CryptoMinDTO) GetCreatedAt() time.Time {
	return c.CreatedAt
}

func (c *CryptoMinDTO) SetUpdatedAt(updatedAt time.Time) *CryptoMinDTO {
	c.UpdatedAt = updatedAt
	return c
}

func (c *CryptoMinDTO) GetUpdatedAt() time.Time {
	return c.UpdatedAt
}

func (c *CryptoMinDTO) SetDeletedAt(deletedAt time.Time) *CryptoMinDTO {
	c.DeletedAt.Time.Time = deletedAt
	c.DeletedAt.Time.Valid = true
	return c
}

func (c *CryptoMinDTO) GetDeletedAt() time.Time {
	return c.DeletedAt.Time.Time
}
