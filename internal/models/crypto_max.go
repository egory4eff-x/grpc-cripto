package models

import (
	"time"

	"gitlab.com/egory4eff-x/grpc-crypto/internal/infrastructure/db/types"
)

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index

type CryptoMaxDTO struct {
	ID        int               `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name      types.NullString  `json:"name" db:"name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	BuyPrice  types.NullString  `json:"buy_price" db:"buy_price" db_type:"varchar(200)" db_default:"default null"  db_ops:"create,update"`
	SellPrice types.NullString  `json:"sell_price" db:"sell_price" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	LastTrade types.NullString  `json:"last_trade" db:"last_trade" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	High      types.NullFloat64 `json:"high" db:"high" db_type:"numeric" db_default:"default 0" db_ops:"create,update"`
	Low       types.NullFloat64 `json:"low" db:"low" db_type:"numeric" db_default:"default 0" db_ops:"create,update"`
	Avg       types.NullString  `json:"avg" db:"avg" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	Vol       types.NullString  `json:"vol" db:"vol" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	VolCurr   types.NullString  `json:"vol_curr" db:"vol_curr" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	Updated   int               `json:"updated" db:"updated" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	CreatedAt time.Time         `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_ops:"created_at"`
	UpdatedAt time.Time         `json:"updated_at" db:"updated_at" db_type:"timestamp" db_default:"default (now()) not null" db_ops:"updated_at"`
	DeletedAt types.NullTime    `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_ops:"deleted_at"`
}

func (c *CryptoMaxDTO) TableName() string {

	return "cryptoMax"
}

func (c *CryptoMaxDTO) OnCreate() []string {
	return []string{}
}

func (c *CryptoMaxDTO) SetID(id int) *CryptoMaxDTO {
	c.ID = id
	return c
}

func (c *CryptoMaxDTO) GetID() int {
	return c.ID
}

func (c *CryptoMaxDTO) SetName(name string) *CryptoMaxDTO {
	c.Name = types.NewNullString(name)
	return c
}

func (c *CryptoMaxDTO) GetName() string {
	return c.Name.String
}

func (c *CryptoMaxDTO) SetBuyPrice(BuyPrice string) *CryptoMaxDTO {
	c.BuyPrice = types.NewNullString(BuyPrice)
	return c
}

func (c *CryptoMaxDTO) GetBuyPrice() string {
	return c.BuyPrice.String
}

func (c *CryptoMaxDTO) SetSellPrice(SellPrice string) *CryptoMaxDTO {
	c.SellPrice = types.NewNullString(SellPrice)
	return c
}

func (c *CryptoMaxDTO) GetSellPrice() string {
	return c.SellPrice.String
}

func (c *CryptoMaxDTO) SetLastTrade(LastTrade string) *CryptoMaxDTO {
	c.LastTrade = types.NewNullString(LastTrade)
	return c
}

func (c *CryptoMaxDTO) GetLastTrade() string {
	return c.LastTrade.String
}

func (c *CryptoMaxDTO) SetHigh(High float64) *CryptoMaxDTO {
	c.High = types.NewNullFloat64(High)
	return c
}

func (c *CryptoMaxDTO) GetHigh() float64 {
	return c.High.Float64
}

func (c *CryptoMaxDTO) SetUpdated(Updated int) *CryptoMaxDTO {
	c.Updated = Updated
	return c
}

func (c *CryptoMaxDTO) GetUpdated() int {
	return c.Updated
}

func (c *CryptoMaxDTO) SetLow(Low float64) *CryptoMaxDTO {
	c.Low = types.NewNullFloat64(Low)
	return c
}

func (c *CryptoMaxDTO) GetLow() float64 {
	return c.Low.Float64
}

func (c *CryptoMaxDTO) SetAvg(Avg string) *CryptoMaxDTO {
	c.Avg = types.NewNullString(Avg)
	return c
}

func (c *CryptoMaxDTO) GetAvg() string {
	return c.Avg.String
}

func (c *CryptoMaxDTO) SetVol(Vol string) *CryptoMaxDTO {
	c.Vol = types.NewNullString(Vol)
	return c
}

func (c *CryptoMaxDTO) GetVol() string {
	return c.Vol.String
}

func (c *CryptoMaxDTO) SetVolCurr(VolCurr string) *CryptoMaxDTO {
	c.VolCurr = types.NewNullString(VolCurr)
	return c
}

func (c *CryptoMaxDTO) GetVolCurr() string {
	return c.VolCurr.String
}

func (c *CryptoMaxDTO) SetCreatedAt(createdAt time.Time) *CryptoMaxDTO {
	c.CreatedAt = createdAt
	return c
}

func (c *CryptoMaxDTO) GetCreatedAt() time.Time {
	return c.CreatedAt
}

func (c *CryptoMaxDTO) SetUpdatedAt(updatedAt time.Time) *CryptoMaxDTO {
	c.UpdatedAt = updatedAt
	return c
}

func (c *CryptoMaxDTO) GetUpdatedAt() time.Time {
	return c.UpdatedAt
}

func (c *CryptoMaxDTO) SetDeletedAt(deletedAt time.Time) *CryptoMaxDTO {
	c.DeletedAt.Time.Time = deletedAt
	c.DeletedAt.Time.Valid = true
	return c
}

func (c *CryptoMaxDTO) GetDeletedAt() time.Time {
	return c.DeletedAt.Time.Time
}
