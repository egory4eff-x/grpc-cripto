package models

import "encoding/json"

type Coin struct {
	ID        int64   `json:"id"`
	Name      string  `json:"name"`
	BuyPrice  string  `json:"buy_price"`
	SellPrice string  `json:"sell_price"`
	LastTrade string  `json:"last_trade"`
	High      float64 `json:"high,string"`
	Low       float64 `json:"low,string"`
	Avg       string  `json:"avg"`
	Vol       string  `json:"vol"`
	VolCurr   string  `json:"vol_curr"`
	Updated   int64   `json:"updated"`
}

type Coins map[string]CoinsValue

func UnmarshalCoins(data []byte) (Coins, error) {
	var r Coins
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Coins) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type CoinsValue struct {
	BuyPrice  string  `json:"buy_price"`
	SellPrice string  `json:"sell_price"`
	LastTrade string  `json:"last_trade"`
	High      float64 `json:"high,string"`
	Low       float64 `json:"low,string"`
	Avg       string  `json:"avg"`
	Vol       string  `json:"vol"`
	VolCurr   string  `json:"vol_curr"`
	Updated   int64   `json:"updated"`
}
