package storages

import (
	"gitlab.com/egory4eff-x/grpc-crypto/internal/db/adapter"
	"gitlab.com/egory4eff-x/grpc-crypto/internal/infrastructure/cache"
	"gitlab.com/egory4eff-x/grpc-crypto/internal/modules/crypto/storage"
)

type Storages struct {
	Crypto storage.Crypter
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		Crypto: storage.NewCryptoStorage(sqlAdapter, cache),
	}
}
