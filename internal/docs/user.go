package docs

import (
	"gitlab.com/egory4eff-x/grpc-crypto/internal/modules/crypto/controller"
)

// swagger:route GET /api/1/crypto/cost crypto cryptoCostRequest
// Получение тикера цен на криптовалюту.
// security:
//   - Bearer: []
// responses:
//   200: cryptoCostResponse

// swagger:response cryptoCostResponse
//
//nolint:all
type cryptoCostResponse struct {
	// in:body
	Body []controller.CryptoResponse
}

// swagger:route GET /api/1/crypto/history crypto cryptoHistoryRequest
// Хранение истории цен криптовалюты.
// security:
//   - Bearer: []
// responses:
//   200: cryptoHistoryResponse

// swagger:response cryptoHistoryResponse
//
//nolint:all
type cryptoHistoryResponse struct {
	// in:body
	Body []controller.CryptoResponse
}

// swagger:route GET /api/1/crypto/max crypto cryptoMaxRequest
// Получение списка пар криптовалют с максимальной ценой.
// security:
//   - Bearer: []
// responses:
//   200: cryptoMaxResponse

// swagger:response cryptoMaxResponse
//
//nolint:all
type cryptoMaxResponse struct {
	// in:body
	Body []controller.CryptoResponse
}

// swagger:route GET /api/1/crypto/min crypto cryptoMinRequest
// Получение списка пар криптовалют с минимальной ценой.
// security:
//   - Bearer: []
// responses:
//   200: cryptoMinResponse

// swagger:response cryptoMinResponse
//
//nolint:all
type cryptoMinResponse struct {
	// in:body
	Body []controller.CryptoResponse
}

// swagger:route GET /api/1/crypto/avg crypto cryptoAvgRequest
// Вывод списка пар криптовалют со средней ценой.
// security:
//   - Bearer: []
// responses:
//   200: cryptoAvgResponse

// swagger:response cryptoAvgResponse
//
//nolint:all
type cryptoAvgResponse struct {
	// in:body
	Body []controller.CryptoResponse
}
