package modules

import (
	"gitlab.com/egory4eff-x/grpc-crypto/internal/infrastructure/component"
	"gitlab.com/egory4eff-x/grpc-crypto/internal/modules/crypto/service"
	"gitlab.com/egory4eff-x/grpc-crypto/internal/storages"
)

type Services struct {
	Crypto service.Crypter
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	cryptoService := service.NewCryptoService(storages.Crypto, components.Logger)
	return &Services{Crypto: cryptoService}
}
